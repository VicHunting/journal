# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import api.models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='CustomUser',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, primary_key=True, serialize=False)),
                ('password', models.CharField(max_length=128, verbose_name='password')),
                ('last_login', models.DateTimeField(verbose_name='last login', null=True, blank=True)),
                ('email', models.EmailField(max_length=254, verbose_name='email address', unique=True)),
                ('is_superuser', models.BooleanField(verbose_name='superuser status', default=False)),
                ('is_editor', models.BooleanField(verbose_name='editor status', default=False)),
                ('is_author', models.BooleanField(verbose_name='author status', default=True)),
            ],
            options={
                'abstract': False,
            },
            managers=[
                ('objects', api.models.CustomUserManager()),
            ],
        ),
    ]
