from django.db import models
from django.conf import settings
from django.db.models.signals import post_save
from django.dispatch import receiver

from rest_framework.authtoken.models import Token

from django.contrib.auth.models import BaseUserManager, AbstractBaseUser


###############################################################################
#                           CUSTOM USER MODEL
###############################################################################

class UserQuerySet(models.QuerySet):
    '''Custom querysets for CustomUser'''

    def authors(self):
        return self.filter(is_author=True)

    def editors(self):
        return self.filter(is_editor=True)


class CustomUserManager(BaseUserManager):
    '''Custom model manager for CustomUser'''
    use_in_migrations = True

    def get_queryset(self):
        return UserQuerySet(self.model, using=self._db)

    def authors(self):
        return self.get_queryset().authors()

    def editors(self):
        return self.get_queryset().editors()

    def _create_user(self, email, password, is_superuser=False,
                     is_editor=False, is_author=True,
                     **kwargs):
        """
        Creates and saves a User with the given email and password.
        """
        if not email:
            raise ValueError('The given user email must be set!')
        email = self.normalize_email(email)
        user = self.model(email=email,
                          is_superuser=is_superuser,
                          is_editor=is_editor,
                          is_author=is_author,
                          **kwargs)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, email, password, is_editor, is_author, **kwargs):
        return self._create_user(email, password, is_superuser=False,
                                 is_editor=False, is_author=True,
                                 **kwargs)

    def create_superuser(self, email, password, **kwargs):
        return self._create_user(email, password, is_superuser=False, **kwargs)


class CustomUser(AbstractBaseUser):
    """Our custom wystem wide User model."""
    email = models.EmailField('email address', unique=True)
    is_superuser = models.BooleanField('superuser status', default=False)
    is_editor = models.BooleanField('editor status', default=False)
    is_author = models.BooleanField('author status', default=True)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['is_editor', 'is_author']
    objects = CustomUserManager()

    def get_full_name(self):
        return self.email

    def get_short_name(self):
        return self.email


@receiver(post_save, sender=settings.AUTH_USER_MODEL)
def create_auth_token(sender, instance=None, created=False, **kwargs):
    '''Automate generating tokens for new users.'''
    if created:
        Token.objects.create(user=instance)


###############################################################################
#                               POST MODEL
###############################################################################

class ConfirmedPostManager(models.Manager):
    def get_queryset(self):
        return super(ConfirmedPostManager, self).get_queryset()\
            .filter(is_confirmed=True)


class UnConfirmedPostManager(models.Manager):
    def get_queryset(self):
        return super(UnConfirmedPostManager, self).get_queryset()\
            .filter(is_confirmed=False)


class Post(models.Model):
    owner = models.ForeignKey(settings.AUTH_USER_MODEL,
                              related_name='posts')
    title = models.CharField(max_length=255)
    body = models.TextField(blank=True, null=True)
    is_confirmed = models.BooleanField('confirmation status', default=False)

    objects = models.Manager()
    confirmed_only = ConfirmedPostManager()
    unconfirmed = UnConfirmedPostManager()

    def __str__(self):
        return self.title
