from django.contrib.auth import get_user_model
from rest_framework import serializers
from djoser.serializers import UserSerializer
from api.models import Post

User = get_user_model()


class PostSerializer(serializers.ModelSerializer):
    # owner = serializers.ReadOnlyField(source='owner.email')
    # owner = UserSerializer(many=False, read_only=False, required=True)
    owner = serializers.SlugRelatedField(
        slug_field='email',
        queryset=User.objects.all(),
    )

    class Meta:
        model = Post
        # fields = ('title', 'owner', 'body', 'is_confirmed')
