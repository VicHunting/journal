from django.conf.urls import include, url

# from rest_framework.urlpatterns import format_suffix_patterns

from api import views

urlpatterns = [
    # url(r'^auth/', include('djoser.urls')),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'^auth/', include('djoser.urls.authtoken')),
    url(r'^posts/$', views.PostList.as_view()),
    url(r'^posts/(?P<pk>[0-9]+)/$', views.PostDetail.as_view()),
]

# urlpatterns = format_suffix_patterns(urlpatterns)
