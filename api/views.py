from api.models import Post 
from api.serializers import PostSerializer
from api.permissions import IsOwnerOrIsAuthorOrReadOnly

from django.http import Http404
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework.permissions import IsAuthenticated


class PostList(APIView):
    """List all posts, or create a new one.

    If logined user is an editor, list all posts, but unconfirmed
    follow at first.

    If logined uset is an author, list all posts of the author, but
    unconfirmed at first. Then list all confirmed posts of other users.

    List all confirmed posts for anonymous (not logined) users.
    """

    def get(self, request, format=None):
        confirmed_posts = Post.confirmed_only.all()

        if request.user.is_anonymous():
            # return all confirmed posts:
            serializer = PostSerializer(confirmed_posts, many=True)
            return Response(serializer.data)

        elif request.user.is_editor:
            # return all posts, but unconfirmed ones first:
            posts = Post.objects.all().order_by('is_confirmed')
            serializer = PostSerializer(posts, many=True)
            return Response(serializer.data)

        elif request.user.is_author:
            authors_posts = Post.objects.filter(
                owner=request.user).order_by('is_confirmed')
            posts = authors_posts | confirmed_posts
            serializer = PostSerializer(posts, many=True)
            return Response(serializer.data)

    def post(self, request, format=None):
        # set owner of the post:
        request.data.update({'owner': request.user})
        # is_confirmed must be False in all new posts:
        request.data.update({'is_confirmed': False})
        serializer = PostSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class PostDetail(APIView):
    """
    Retrieve, update or delete a Post instance.

    Owners of the post or editors may edit the object.
    """

    permission_classes = (IsAuthenticated,
                          IsOwnerOrIsAuthorOrReadOnly)

    def get_object(self, pk):
        try:
            obj = Post.objects.get(pk=pk)
            self.check_object_permissions(self.request, obj)
            return obj
        except Post.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        post = self.get_object(pk)

        if request.user.is_editor:
            serializer = PostSerializer(post)
            return Response(serializer.data)

        if request.user.is_author:
            if post.owner.email == request.user.email:
                serializer = PostSerializer(post)
                return Response(serializer.data)
            else:
                return Response(status.HTTP_403_FORBIDDEN)

        if request.user.is_anonymous():
            if post.is_confirmed:
                serializer = PostSerializer(post)
                return Response(serializer.data)
            else:
                return Response(status.HTTP_403_FORBIDDEN)

    def put(self, request, pk, format=None):
        post = self.get_object(pk)
        serializer = PostSerializer(post, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        post = self.get_object(pk)
        post.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
